/*
javascript para block academy
*/


//Cambia textos de la nav-bar.
document.addEventListener("DOMContentLoaded", function(){
  const enlaces = document.querySelectorAll('.primary-navigation ul .nav-link');
  const nuevosTextos = ['Pendientes', 'Training Camp', 'Ajustes del Sitio', 'Guía'];
  enlaces.forEach(function(enlace, index) {
    enlace.innerHTML = nuevosTextos[index];
  });
});

/* menu lateral */

document.addEventListener("DOMContentLoaded", function(){
  var html = `<div id="ba-botonera">
                <ul class="nav flex-column">
                  <li>
                    <a href="#link1"> 
                      <i class="fa fa-graduation-cap" aria-hidden="true"></i> Training Camp
                    </a>
                  </li>
                  <li>
                    <a href="#"> 
                      <i class="fa fa-comment" aria-hidden="true"></i> Foro
                    </a>
                  </li>
                  <li>
                    <a href="#"> 
                      <i class="fa fa-calendar" aria-hidden="true"></i> Calendario
                    </a>
                  </li>
                  <li>
                    <a href="#"> 
                      <i class="fa fa-money" aria-hidden="true"></i> Wallet
                    </a>
                  </li>
                  <li>
                    <a href="#"> 
                      <i class="fa fa-book" aria-hidden="true"></i> Mis Certificaciones
                    </a>
                  </li>
                  <li>
                    <a href="#"> 
                      <i class="fa fa-trophy" aria-hidden="true"></i> Ranking
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-question-circle" aria-hidden="true"></i> Ayuda
                    </a>
                  </li>
                </ul>
              </div>`;
  $('.drawer-left .drawercontent').append(html);
  //fin barra BA
  //-----------------------------
  //cambiar link a btn de perfil
  
  document.querySelector('#usermenu-carousel #carousel-item-main .dropdown-item:first-child').href = 'https://beta-qa.blockacademy.gg/en-US/profile';
  document.querySelector('#usermenu-carousel #carousel-item-main .dropdown-item:first-child').target='_blank';

  /*
  -----------------------------
  - postear pregunta en foros -
  -----------------------------
  */

  if (document.body.id === 'page-mod-moodleoverflow-view') {
    const newdiscussionform = document.getElementById('newdiscussionform');
    //const ba_newdiscussionform = document.getElementById('ba_newdiscussionform');
    const ba_newdiscussionform =  document.querySelector('#page-mod-moodleoverflow-view div[role="main"] #intro .ba-course_header form#ba_newdiscussionform');

    const moodleOverflowInput = newdiscussionform.querySelector('input[name="moodleoverflow"]');
    const newdiscussioInput = ba_newdiscussionform.querySelector('input[name="moodleoverflow"]');
  
    // Copiar el valor del atributo "value" del input de newdiscussionform al input de ba-pos
    newdiscussioInput.value = moodleOverflowInput.value;
  }

  //modificar menus en foros prueba de enlaces

  if (document.body.id === "page-mod-moodleoverflow-discussion") {
    // Obtener el elemento "post"
    var posteos = document.querySelector('#moodleoverflow-posts');
  
    // Obtener el elemento "post-info" dentro del elemento "posteos"
    var postInfos = posteos.querySelectorAll('.moodleoverflowpost');
  
    // Recorrer todos los elementos "moodleoverflowpost"
    postInfos.forEach(function(postInfo) {
      // Obtener el elemento "post-menu" dentro de cada elemento "post-info"
      var postMenu = postInfo.querySelector('.post-info .post-menu');
  
      if (postMenu) {
        // Clonar el elemento "post-menu"
        var postMenuClone = postMenu.cloneNode(true);
        postMenuClone.classList.add('BA-foromenu');
  
        // Obtener el elemento "moodleoverflowpost" que contiene el elemento "post-menu"
        var moodleOverflowPost = postMenu.closest('.moodleoverflowpost');
  
        // Obtener el elemento "answercell" dentro del elemento "moodleoverflowpost"
        var answerCell = moodleOverflowPost.querySelector('.answercell');
  
        // Insertar el elemento clonado dentro del elemento "answercell"
        answerCell.appendChild(postMenuClone);
      }

      //Cambiar Titulo de lugar
      if (document.body.id === "page-mod-moodleoverflow-discussion") {
        // Verificar si el contenido ya ha sido duplicado
        if (!document.querySelector('.ba-preguntaforo')) {
          // Obtener el elemento "post"
          var post = document.querySelector('div[role="main"]');
          // Obtener el elemento "discussname"
          var discussname = post.querySelector('.discussionname');
          // Obtener el elemento "answercell"
          var answerCell = post.querySelector('.answercell');
          // Clonar el elemento "discussname"
          var discussnameClone = discussname.cloneNode(true);
          discussnameClone.classList.add('BA-titleForo');
          // Crear un nuevo elemento "div"
          var preguntaForoDiv = document.createElement('div');
          // Agregar la clase "ba-preguntaforo" al nuevo elemento "div"
          preguntaForoDiv.classList.add('ba-preguntaforo');
          // Insertar el elemento clonado dentro del nuevo elemento "div"
          preguntaForoDiv.appendChild(discussnameClone);
          // Insertar el nuevo elemento "div" dentro del elemento "answercell"
          answerCell.appendChild(preguntaForoDiv);
        }
      }
      

    });

    //

    const links = document.querySelectorAll('.moodleoverflowpost .BA-foromenu a');

    for (let i = 0; i < links.length; i++) {
      const linkText = links[i].textContent.trim().replace(/\s+/g, '_');
      links[i].classList.add(linkText);
    }
  }


});

document.addEventListener("DOMContentLoaded", function() {

  if (document.body.id === "page-mod-moodleoverflow-view") {
    // Obtener una referencia a la tabla por su clase
    var tabla = document.querySelector(".moodleoverflowheaderlist");

    // Obtener una referencia a los encabezados de columna
    var encabezados = tabla.querySelectorAll("th");

    // Recorrer cada encabezado de columna
    for (var i = 0; i < encabezados.length; i++) {
      // Obtener el nombre de la clase del encabezado de columna
      var clase = encabezados[i].textContent.replace(/\s+/g, "_");

      // Recorrer cada celda de la columna y asignar la clase
      for (var j = 0; j < tabla.rows.length; j++) {
        var celda = tabla.rows[j].cells[i];
        celda.classList.add(clase);
      }
    }
  }
});
